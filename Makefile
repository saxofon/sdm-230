TOP = $(shell pwd)

export DESTDIR=
export PREFIX=/usr

SUBDIRS += src
SUBDIRS += examples

all: $(SUBDIRS)

.PHONY: subdirs $(SUBDIRS)

subdirs: $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@

install:
	$(MAKE) -C src install
	$(MAKE) -C examples install

clean:
	$(MAKE) -C src clean
	$(MAKE) -C examples clean
