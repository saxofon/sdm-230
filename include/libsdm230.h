#ifndef __LIBCRH_H__
#define __LIBCRH_H__

#define L1_VOLTAGE                       0x0000
#define L2_VOLTAGE                       0x0002
#define L3_VOLTAGE                       0x0004
#define L1_CURRENT                       0x0006
#define L2_CURRENT                       0x0008
#define L3_CURRENT                       0x000A
#define L1_ACTIVE_POWER                  0x000C
#define L2_ACTIVE_POWER                  0x000E
#define L3_ACTIVE_POWER                  0x0010
#define L1_APPARENT_POWER                0x0012
#define L2_APPARENT_POWER                0x0014
#define L3_APPARENT_POWER                0x0016
#define L1_REACTIVE_POWER                0x0018
#define L2_REACTIVE_POWER                0x001A
#define L3_REACTIVE_POWER                0x001C
#define L1_POWER_FACTOR                  0x001E
#define L2_POWER_FACTOR                  0x0020
#define L3_POWER_FACTOR                  0x0022
#define L1_PHASE_ANGLE                   0x0024
#define L2_PHASE_ANGLE                   0x0026
#define L3_PHASE_ANGLE                   0x0028
#define AVG_VOLTAGE                      0x002A
#define AVG_CURRENT                      0x002E
#define SUM_CURRENT                      0x0030
#define TOT_SYSTEM_POWER                 0x0034
#define TOT_SYSTEM_APPARENT_POWER        0x0038
#define TOT_SYSTEM_REACTIVE_POWER        0x003C
#define TOT_SYSTEM_POWER_FACTOR          0x003E
#define TOT_SYSTEM_PHASE_ANGLE           0x0042
#define LINE_FREQUENCY                   0x0046
#define IMPORT_ACTIVE_ENERGY             0x0048
#define EXPORT_ACTIVE_ENERGY             0x004A
#define IMPORT_REACTIVE_ENERGY           0x004C
#define EXPORT_REACTIVE_ENERGY           0x004E
#define APPARENT_ENERGY_SINCE_RESET      0x0050
#define REACTIVE_ENERGY_SINCE_RESET      0x0052
#define TOTAL_SYSTEM_POWER_DEMAND        0x0054
#define MAX_TOTAL_SYSTEM_POWER_DEMAND    0x0056
#define CUR_SYSTEM_POSITIVE_POWER_DEMAND 0x0058
#define MAX_SYSTEM_POSITIVE_POWER_DEMAND 0x005A
#define CUR_SYSTEM_REVERSE_POWER_DEMAND  0x005C
#define MAX_SYSTEM_REVERSE_POWER_DEMAND  0x005E
#define TOT_SYSTEM_APPERANT_POWER_DEMAND 0x0064
#define MAX_SYSTEM_APPERANT_POWER_DEMAND 0x0066
#define NEUTRAL_CURRENT_DEMAND           0x0068
#define MAX_NEUTRAL_CURRENT_DEMAND       0x006A
#define L1_TO_L2_VOLTAGE                 0x00C8
#define L2_TO_L3_VOLTAGE                 0x00CA
#define L3_TO_L1_VOLTAGE                 0x00CC
#define AVG_LINE_TO_LINE_VOLTAGE         0x00CE
#define NEUTRAL_CURRENT                  0x00E0
#define L1_VOLTAGE_THD                   0x00EA
#define L2_VOLTAGE_THD                   0x00EC
#define L3_VOLTAGE_THD                   0x00EE
#define L1_CURRENT_THD                   0x00FO
#define L2_CURRENT_THD                   0x00F2
#define L3_CURRENT_THD                   0x00F4
#define AVG_VOLTAGE_THD                  0x00F8
#define AVG_CURRENT_THD                  0x00FA
#define L1_CURRENT_DEMAND                0x0102
#define L2_CURRENT_DEMAND                0x0104
#define L3_CURRENT_DEMAND                0x0106
#define L1_MAX_CURRENT_DEMAND            0x0108
#define L2_MAX_CURRENT_DEMAND            0x010A
#define L3_MAX_CURRENT_DEMAND            0x010C
#define L1_TO_L2_VOLTAGE_THD             0x014E
#define L2_TO_L3_VOLTAGE_THD             0x0150
#define L3_TO_L1_VOLTAGE_THD             0x0152
#define AVG_LINE_TO_LINE_VOLTAGE_THD     0x0154
#define TOTAL_ACTIVE_ENERGY              0x0156
#define TOTAL_REACTIVE_ENERGY            0x0158
#define L1_IMPORT_ACTIVE_ENERGY          0x015A
#define L2_IMPORT_ACTIVE_ENERGY          0x015C
#define L3_IMPORT_ACTIVE_ENERGY          0x015E
#define L1_EXPORT_ACTIVE_ENERGY          0x0160
#define L2_EXPORT_ACTIVE_ENERGY          0x0162
#define L3_EXPORT_ACTIVE_ENERGY          0x0164
#define L1_TOTAL_ACTIVE_ENERGY           0x0166
#define L2_TOTAL_ACTIVE_ENERGY           0x0168
#define L3_TOTAL_ACTIVE_ENERGY           0x016A
#define L1_IMPORT_REACTIVE_ENERGY        0x016C
#define L2_IMPORT_REACTIVE_ENERGY        0x016E
#define L3_IMPORT_REACTIVE_ENERGY        0x0170
#define L1_EXPORT_REACTIVE_ENERGY        0x0172
#define L2_EXPORT_REACTIVE_ENERGY        0x0174
#define L3_EXPORT_REACTIVE_ENERGY        0x0176
#define L1_TOTAL_REACTIVE_ENERGY         0x0178
#define L2_TOTAL_REACTIVE_ENERGY         0x017A
#define L3_TOTAL_REACTIVE_ENERGY         0x017C
#define TOTAL_RESETTABLE_ACTIVE_ENERGY   0x0180
#define TOTAL_RESETTABLE_REACTIVE_ENERGY 0x0182

#define PULSE_1_OUTPUT_WIDTH             0x000C
#define NETWORK_PARITY                   0x0012
#define NETWORK_ID                       0x0014
#define NETWORK_BAUDRATE                 0x001C
#define PULSE_1_ENERGY_TYPE              0x0056
#define DISPLAY_CONFIG                   0xF500
#define PULSE_1_CONSTANT                 0xF910
#define MEASUREMENT_MODE                 0xF920
#define RUNNING_TIME                     0xF930

union u_data {
	uint16_t s[2];
	uint32_t i;
	float    f;
};

struct s_sdm {
	char *interface;
	pthread_t tid;
	int error;
	FILE *f;
	modbus_t *mb_ctx;

	float pulse1_width;
	float network_parity;
	float network_id;
	float network_baudrate;
	float pulse1_energy_type;
	uint16_t display_config;
	uint16_t pulse1_constant;
	uint16_t measurement_mode;
	float running_time;
};

modbus_t *libsdm230_connect(const char *interface, const int baud);
void libsdm230_disconnect(modbus_t *ctx);

void libsdm230_line_voltage(modbus_t *ctx, float L[3]);
void libsdm230_line_current(modbus_t *ctx, float L[3]);
void libsdm230_active_power(modbus_t *ctx, float L[3]);
void libsdm230_apparent_power(modbus_t *ctx, float L[3]);
void libsdm230_reactive_power(modbus_t *ctx, float L[3]);
void libsdm230_power_factor(modbus_t *ctx, float L[3]);
void libsdm230_phase_angle(modbus_t *ctx, float L[3]);
float libsdm230_line_frequency(modbus_t *ctx);
void libsdm230_import_active_energy(modbus_t *ctx, float L[3]);
void libsdm230_export_active_energy(modbus_t *ctx, float L[3]);
void libsdm230_import_reactive_energy(modbus_t *ctx, float L[3]);
void libsdm230_export_reactive_energy(modbus_t *ctx, float L[3]);
float libsdm230_total_system_power_demand(modbus_t *ctx);
float libsdm230_max_total_system_power_demand(modbus_t *ctx);
float libsdm230_cur_system_positive_power_demand(modbus_t *ctx);
float libsdm230_max_system_positive_power_demand(modbus_t *ctx);
float libsdm230_cur_system_reverse_power_demand(modbus_t *ctx);
float libsdm230_max_system_reverse_power_demand(modbus_t *ctx);
void libsdm230_current_demand(modbus_t *ctx, float L[3]);
void libsdm230_max_current_demand(modbus_t *ctx, float L[3]);
void libsdm230_total_active_energy(modbus_t *ctx, float L[3]);
void libsdm230_total_reactive_energy(modbus_t *ctx, float L[3]);
float libsdm230_total_resettable_active_energy(modbus_t *ctx);
float libsdm230_total_resettable_reactive_energy(modbus_t *ctx);

float libsdm230_pulse_1_output_width(modbus_t *ctx);
float libsdm230_network_parity(modbus_t *ctx);
float libsdm230_network_id(modbus_t *ctx);
float libsdm230_network_baudrate(modbus_t *ctx);
float libsdm230_pulse_1_energy_type(modbus_t *ctx);
uint32_t libsdm230_display_config(modbus_t *ctx);
uint16_t libsdm230_pulse_1_constant(modbus_t *ctx);
uint16_t libsdm230_measurement_mode(modbus_t *ctx);
float libsdm230_running_time(modbus_t *ctx);

#endif
