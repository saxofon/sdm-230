#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <stdbool.h>
#include <modbus/modbus.h>
#include <libsdm230.h>

int main(int argc, char *argv[])
{
	union u_data data;
        float V[3];
        float A[3];
        float AP[3];

	modbus_t *ctx = libsdm230_connect("/dev/ttyUSB0", 38400);

	if (!ctx) {
		printf("cannot connect to energy meter\n");
		exit(-1);
	}

	printf("Line voltage current frequency power\n");
	for (;;) {
		printf("Line  Volt      Amp      ActivePower    (frequency : %5.2f)\n", libsdm230_line_frequency(ctx));
		libsdm230_line_voltage(ctx, V);
		libsdm230_line_current(ctx, A);
		libsdm230_active_power(ctx, AP);
		printf("L1    %08.4f  %07.4f  %09.4f\n", V[0], A[0], AP[0]);
		printf("L2    %08.4f  %07.4f  %09.4f\n", V[1], A[1], AP[1]);
		printf("L3    %08.4f  %07.4f  %09.4f\n", V[2], A[2], AP[2]);
		printf("\n\n");
		sleep(1);
	}


	libsdm230_disconnect(ctx);
}
