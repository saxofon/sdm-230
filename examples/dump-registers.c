#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stdbool.h>
#include <modbus/modbus.h>
#include <libsdm230.h>

int main(int argc, char *argv[])
{
	union u_data data;

	modbus_t *ctx = libsdm230_connect("/dev/ttyUSB0");

	if (!ctx) {
		printf("cannot connect to energy meter\n");
		exit(-1);
	}

	//data.f = 2.0;
	//write_holding_reg(ctx, NETWORK_BAUDRATE, data);

	printf("Line voltage                         : %f\n", libsdm230_line_voltage(ctx));
	printf("Line current                         : %f\n", libsdm230_line_current(ctx));
	printf("Active power                         : %f\n", libsdm230_active_power(ctx));
	printf("Apparent power                       : %f\n", libsdm230_apparent_power(ctx));
	printf("Reactive power                       : %f\n", libsdm230_reactive_power(ctx));
	printf("Power factor                         : %f\n", libsdm230_power_factor(ctx));
	printf("Phase angle                          : %f\n", libsdm230_phase_angle(ctx));
	printf("Line frequency                       : %f\n", libsdm230_line_frequency(ctx));
	printf("Import active energy                 : %f\n", libsdm230_import_active_energy(ctx));
	printf("Export active energy                 : %f\n", libsdm230_export_active_energy(ctx));
	printf("Import reactive energy               : %f\n", libsdm230_import_reactive_energy(ctx));
	printf("Export reactive energy               : %f\n", libsdm230_export_reactive_energy(ctx));
	printf("Total system power demand            : %f\n", libsdm230_total_system_power_demand(ctx));
	printf("Max total system power demand        : %f\n", libsdm230_max_total_system_power_demand(ctx));
	printf("Current system positive power demand : %f\n", libsdm230_cur_system_positive_power_demand(ctx));
	printf("Max system positive power demand     : %f\n", libsdm230_max_system_positive_power_demand(ctx));
	printf("Current system reverse power demand  : %f\n", libsdm230_cur_system_reverse_power_demand(ctx));
	printf("Max system reverse power demand      : %f\n", libsdm230_max_system_reverse_power_demand(ctx));
	printf("Current demand                       : %f\n", libsdm230_current_demand(ctx));
	printf("Max Current demand                   : %f\n", libsdm230_max_current_demand(ctx));
	printf("Total active energy                  : %f\n", libsdm230_total_active_energy(ctx));
	printf("Total reactive energy                : %f\n", libsdm230_total_reactive_energy(ctx));
	printf("Total resettable active energy       : %f\n", libsdm230_total_resettable_active_energy(ctx));
	printf("Total resettable reactive energy     : %f\n", libsdm230_total_resettable_reactive_energy(ctx));
	printf("Pulse 1 output width                 : %f\n", libsdm230_pulse_1_output_width(ctx));
	printf("Network parity                       : %f\n", libsdm230_network_parity(ctx));
	printf("Network id                           : %f\n", libsdm230_network_id(ctx));
	printf("Network baudrate                     : %f\n", libsdm230_network_baudrate(ctx));
	printf("Pulse 1 energy type                  : %f\n", libsdm230_pulse_1_energy_type(ctx));
	printf("Display config                       : %X\n", libsdm230_display_config(ctx));
	printf("Pulse 1 constant                     : %d\n", libsdm230_pulse_1_constant(ctx));
	printf("Measurment mode                      : %X\n", libsdm230_measurement_mode(ctx));
	printf("Running time                         : %f\n", libsdm230_running_time(ctx));


	libsdm230_disconnect(ctx);
}
