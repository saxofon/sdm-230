[per@yacht examples]$ sudo make test-dump-registers 
LD_LIBRARY_PATH=../src ./dump-registers
Line voltage                         : 212.048065
Line current                         : 9.030908
Active power                         : 1917.778809
Apparent power                       : 1918.193359
Reactive power                       : 39.859741
Power factor                         : 0.999784
Phase angle                          : 1.190979
Line frequency                       : 49.951218
Import active energy                 : 712.765015
Export active energy                 : 0.000000
Import reactive energy               : 11.968000
Export reactive energy               : 9.922000
Total system power demand            : 1695.510864
Max total system power demand        : 2842.841553
Current system positive power demand : 1695.510864
Max system positive power demand     : 2842.841553
Current system reverse power demand  : 0.000000
Max system reverse power demand      : 0.077311
Current demand                       : 7.960204
Max Current demand                   : 14.937644
Total active energy                  : 712.765991
Total reactive energy                : 21.889999
Total resettable active energy       : 712.765991
Total resettable reactive energy     : 21.889999
Pulse 1 output width                 : 100.000000
Network parity                       : 0.000000
Network id                           : 1.000000
Network baudrate                     : 2.000000
Pulse 1 energy type                  : 4.000000
Display config                       : 15010060
Pulse 1 constant                     : 0
Measurment mode                      : 2
Running time                         : 1532.933350
[per@yacht examples]$ 
