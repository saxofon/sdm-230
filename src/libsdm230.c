#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stdbool.h>
#include <modbus/modbus.h>
#include "libsdm230.h"

static float read_input_reg(modbus_t *ctx, uint16_t reg)
{
    int nreg;
    uint16_t reg_data[2];
    union u_data data;

    nreg = modbus_read_input_registers(ctx, reg, 2, reg_data);

    if (nreg == -1) {
       fprintf(stderr, "Error reading registers: %s\n", modbus_strerror(errno));
       return 0.0;
    } else {
	data.s[0] = reg_data[1];
	data.s[1] = reg_data[0];
	return data.f;
    }
}

static union u_data read_holding_reg(modbus_t *ctx, uint16_t reg)
{
    int nreg;
    uint16_t reg_data[2];
    union u_data data;

    nreg = modbus_read_registers(ctx, reg, 2, reg_data);

    if (nreg == -1) {
       fprintf(stderr, "Error reading registers: %s\n", modbus_strerror(errno));
       return data;
    } else {
	data.s[0] = reg_data[1];
	data.s[1] = reg_data[0];
	return data;
    }
}

static float read_holding_reg_float(modbus_t *ctx, uint16_t reg)
{
	union u_data data = read_holding_reg(ctx, reg);
	return data.f;
}

static unsigned int read_holding_reg_int(modbus_t *ctx, uint16_t reg)
{
	union u_data data = read_holding_reg(ctx, reg);
	return data.i;
}

static int write_holding_reg(modbus_t *ctx, uint16_t reg, union u_data data)
{
	int nreg;
	uint16_t reg_data[2];

	printf("Setting 0x%X to %f\n", reg, data.f);
	reg_data[1] = data.s[0];
	reg_data[0] = data.s[1];

	nreg = modbus_write_registers(ctx, reg, 2, reg_data);

	if (nreg == -1) {
		fprintf(stderr, "Error writing register: %s\n", modbus_strerror(errno));
		exit(-1);
	}
}


modbus_t *libsdm230_connect(const char *interface, const int baud)
{
	modbus_t *ctx = NULL;
	struct timeval old_response_timeout;
	struct timeval response_timeout;

	ctx = modbus_new_rtu(interface, baud, 'N', 8, 1);
	if (!ctx) {
		fprintf(stderr, "Unable to create the libmodbus context\n");
		return NULL;
	}

	if (modbus_set_slave(ctx, 1) == -1) {
		fprintf(stderr, "Couldn't set slave: %s\n", modbus_strerror(errno));
		modbus_free(ctx);
		return NULL;
	}

	if (modbus_connect(ctx) == -1) {
		fprintf(stderr, "Connection failed: %s\n", modbus_strerror(errno));
		modbus_free(ctx);
		return NULL;
	}

	return ctx;
}

void libsdm230_disconnect(modbus_t *ctx)
{
	modbus_close(ctx);
	modbus_free(ctx);
}

void libsdm230_line_voltage(modbus_t *ctx, float L[3])
{
	L[0] = read_input_reg(ctx, L1_VOLTAGE);
	L[1] = read_input_reg(ctx, L2_VOLTAGE);
	L[2] = read_input_reg(ctx, L3_VOLTAGE);
}

void libsdm230_line_current(modbus_t *ctx, float L[3])
{
	L[0] = read_input_reg(ctx, L1_CURRENT);
	L[1] = read_input_reg(ctx, L1_CURRENT);
	L[2] = read_input_reg(ctx, L1_CURRENT);
}

void libsdm230_active_power(modbus_t *ctx, float L[3])
{
	L[0] = read_input_reg(ctx, L1_ACTIVE_POWER);
	L[1] = read_input_reg(ctx, L2_ACTIVE_POWER);
	L[2] = read_input_reg(ctx, L3_ACTIVE_POWER);
}

void libsdm230_apparent_power(modbus_t *ctx, float L[3])
{
	L[0] = read_input_reg(ctx, L1_APPARENT_POWER);
	L[1] = read_input_reg(ctx, L2_APPARENT_POWER);
	L[2] = read_input_reg(ctx, L3_APPARENT_POWER);
}

void libsdm230_reactive_power(modbus_t *ctx, float L[3])
{
	L[0] = read_input_reg(ctx, L1_REACTIVE_POWER);
	L[1] = read_input_reg(ctx, L2_REACTIVE_POWER);
	L[2] = read_input_reg(ctx, L3_REACTIVE_POWER);
}

void libsdm230_power_factor(modbus_t *ctx, float L[3])
{
	L[0] = read_input_reg(ctx, L1_POWER_FACTOR);
	L[1] = read_input_reg(ctx, L2_POWER_FACTOR);
	L[2] = read_input_reg(ctx, L3_POWER_FACTOR);
}

void libsdm230_phase_angle(modbus_t *ctx, float L[3])
{
	L[0] = read_input_reg(ctx, L1_PHASE_ANGLE);
	L[1] = read_input_reg(ctx, L2_PHASE_ANGLE);
	L[2] = read_input_reg(ctx, L3_PHASE_ANGLE);
}

float libsdm230_line_frequency(modbus_t *ctx)
{
	return read_input_reg(ctx, LINE_FREQUENCY);
}

void libsdm230_import_active_energy(modbus_t *ctx, float L[3])
{
	L[0] = read_input_reg(ctx, L1_IMPORT_ACTIVE_ENERGY);
	L[1] = read_input_reg(ctx, L2_IMPORT_ACTIVE_ENERGY);
	L[2] = read_input_reg(ctx, L3_IMPORT_ACTIVE_ENERGY);
}

void libsdm230_export_active_energy(modbus_t *ctx, float L[3])
{
	L[0] = read_input_reg(ctx, L1_EXPORT_ACTIVE_ENERGY);
	L[1] = read_input_reg(ctx, L2_EXPORT_ACTIVE_ENERGY);
	L[2] = read_input_reg(ctx, L3_EXPORT_ACTIVE_ENERGY);
}

void libsdm230_import_reactive_energy(modbus_t *ctx, float L[3])
{
	L[0] = read_input_reg(ctx, L1_IMPORT_REACTIVE_ENERGY);
	L[1] = read_input_reg(ctx, L2_IMPORT_REACTIVE_ENERGY);
	L[2] = read_input_reg(ctx, L3_IMPORT_REACTIVE_ENERGY);
}

void libsdm230_export_reactive_energy(modbus_t *ctx, float L[3])
{
	L[0] = read_input_reg(ctx, L1_EXPORT_REACTIVE_ENERGY);
	L[1] = read_input_reg(ctx, L2_EXPORT_REACTIVE_ENERGY);
	L[2] = read_input_reg(ctx, L3_EXPORT_REACTIVE_ENERGY);
}

float libsdm230_total_system_power_demand(modbus_t *ctx)
{
	return read_input_reg(ctx, TOTAL_SYSTEM_POWER_DEMAND);
}

float libsdm230_max_total_system_power_demand(modbus_t *ctx)
{
	return read_input_reg(ctx, MAX_TOTAL_SYSTEM_POWER_DEMAND);
}

float libsdm230_cur_system_positive_power_demand(modbus_t *ctx)
{
	return read_input_reg(ctx, CUR_SYSTEM_POSITIVE_POWER_DEMAND);
}

float libsdm230_max_system_positive_power_demand(modbus_t *ctx)
{
	return read_input_reg(ctx, MAX_SYSTEM_POSITIVE_POWER_DEMAND);
}

float libsdm230_cur_system_reverse_power_demand(modbus_t *ctx)
{
	return read_input_reg(ctx, CUR_SYSTEM_REVERSE_POWER_DEMAND);
}

float libsdm230_max_system_reverse_power_demand(modbus_t *ctx)
{
	return read_input_reg(ctx, MAX_SYSTEM_REVERSE_POWER_DEMAND);
}

void libsdm230_current_demand(modbus_t *ctx, float L[3])
{
	L[0] = read_input_reg(ctx, L1_CURRENT_DEMAND);
	L[1] = read_input_reg(ctx, L2_CURRENT_DEMAND);
	L[2] = read_input_reg(ctx, L3_CURRENT_DEMAND);
}

void libsdm230_max_current_demand(modbus_t *ctx, float L[3])
{
	L[0] = read_input_reg(ctx, L1_MAX_CURRENT_DEMAND);
	L[1] = read_input_reg(ctx, L2_MAX_CURRENT_DEMAND);
	L[2] = read_input_reg(ctx, L3_MAX_CURRENT_DEMAND);
}

void libsdm230_total_active_energy(modbus_t *ctx, float L[3])
{
	L[0] = read_input_reg(ctx, L1_TOTAL_ACTIVE_ENERGY);
	L[1] = read_input_reg(ctx, L2_TOTAL_ACTIVE_ENERGY);
	L[2] = read_input_reg(ctx, L3_TOTAL_ACTIVE_ENERGY);
}

void libsdm230_total_reactive_energy(modbus_t *ctx, float L[3])
{
	L[0] = read_input_reg(ctx, L1_TOTAL_REACTIVE_ENERGY);
	L[1] = read_input_reg(ctx, L2_TOTAL_REACTIVE_ENERGY);
	L[2] = read_input_reg(ctx, L3_TOTAL_REACTIVE_ENERGY);
}

float libsdm230_total_resettable_active_energy(modbus_t *ctx)
{
	return read_input_reg(ctx, TOTAL_RESETTABLE_ACTIVE_ENERGY);
}

float libsdm230_total_resettable_reactive_energy(modbus_t *ctx)
{
	return read_input_reg(ctx, TOTAL_RESETTABLE_REACTIVE_ENERGY);
}

float libsdm230_pulse_1_output_width(modbus_t *ctx)
{
	return read_holding_reg_float(ctx, PULSE_1_OUTPUT_WIDTH);
}

float libsdm230_network_parity(modbus_t *ctx)
{
	return read_holding_reg_float(ctx, NETWORK_PARITY);
}

float libsdm230_network_id(modbus_t *ctx)
{
	return read_holding_reg_float(ctx, NETWORK_ID);
}

float libsdm230_network_baudrate(modbus_t *ctx)
{
	return read_holding_reg_float(ctx, NETWORK_BAUDRATE);
}

float libsdm230_pulse_1_energy_type(modbus_t *ctx)
{
	return read_holding_reg_float(ctx, PULSE_1_ENERGY_TYPE);
}

uint32_t libsdm230_display_config(modbus_t *ctx)
{
	return read_holding_reg_int(ctx, DISPLAY_CONFIG);
}

uint16_t libsdm230_pulse_1_constant(modbus_t *ctx)
{
	return read_holding_reg(ctx, PULSE_1_CONSTANT).s[1];
}

uint16_t libsdm230_measurement_mode(modbus_t *ctx)
{
	return read_holding_reg(ctx, MEASUREMENT_MODE).s[1];
}

float libsdm230_running_time(modbus_t *ctx)
{
	return read_holding_reg_float(ctx, RUNNING_TIME);
}
